/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;


/**
 *
 * @author Amagi
 */
@Named(value = "bukuBean")
@SessionScoped
public class BukuBean implements Serializable{

    public List<Buku> getBuku() throws SQLException, ClassNotFoundException{
        Connection connect = null;
        
        String url = "jdbc:mysql://localhost:3306/minilib";
        
        String username = "root";
        String password = "";
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            
            connect = DriverManager.getConnection(url, username, password);
        } catch(SQLException ex){
            System.out.println("in exec");
            System.out.println(ex.getMessage());
        }
        
        List<Buku> buku = new ArrayList<Buku>();
        String query = "Select * from buku";
        Statement st = connect.createStatement();
        ResultSet rs = st.executeQuery(query);
        
        while(rs.next()){
            String id = rs.getString("id");
            String judul = rs.getString("judul");
            String kategori = rs.getString("kategori");
            String harga = rs.getString("harga");
            Buku b = new Buku(id, judul, kategori, harga);
            
            buku.add(b);
        }
        
        rs.close();
        st.close();
        connect.close();
        
        return buku;
    }
    
    public void tambahBuku(String id, String judul, String kategori, String harga) throws ClassNotFoundException, SQLException{
        Connection connect = null;
        
        String url = "jdbc:mysql://localhost:3306/minilib";
        
        String username = "root";
        String password = "";
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            
            connect = DriverManager.getConnection(url, username, password);
        } catch(SQLException ex){
            System.out.println("in exec");
            System.out.println(ex.getMessage());
        }
        String nama;
        harga = "Rp. "+harga+",00";
        String query = "INSERT INTO `buku`(`ID`, `judul`, `kategori`, `harga`) VALUES ('"+id+"', '"+judul+"', '"+kategori+"', '"+harga+"');";
        System.out.println(query);
        Statement st = connect.createStatement();
        st.executeQuery(query);
    }
    
}
