/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author Amagi
 */
@Named(value = "penyewaBean")
@SessionScoped
public class PenyewaBean implements Serializable{

    public List<Penyewa> getPenyewa() throws SQLException, ClassNotFoundException {
        Connection connect = null;
        
        String url = "jdbc:mysql://localhost:3306/minilib";
        
        String username = "root";
        String password = "";
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            
            connect = DriverManager.getConnection(url, username, password);
        } catch(SQLException ex){
            System.out.println("in exec");
            System.out.println(ex.getMessage());
        }
        
        List<Penyewa> penyewa = new ArrayList<Penyewa>();
        String query = "Select * from penyewa";
        Statement st = connect.createStatement();
        ResultSet rs = st.executeQuery(query);
        
        while(rs.next()){
            String id = rs.getString("id");
            String nama = rs.getString("nama");
            String alamat = rs.getString("alamat");
            Penyewa p = new Penyewa(id, nama, alamat);
            
            penyewa.add(p);
        }
        
        rs.close();
        st.close();
        connect.close();
        
        return penyewa;
    }
    
    public void deletePenyewa(int id) throws ClassNotFoundException, SQLException{
        Connection connect = null;
        
        String url = "jdbc:mysql://localhost:3306/minilib";
        
        String username = "root";
        String password = "";
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            
            connect = DriverManager.getConnection(url, username, password);
        } catch(SQLException ex){
            System.out.println("in exec");
            System.out.println(ex.getMessage());
        }
        
        String query = "delete from penyewa where id="+id;
        Statement st = connect.createStatement();
        ResultSet rs = st.executeQuery(query);
        
    }
    
    public void tambahPenyewa(String id, String nama, String alamat) throws SQLException, ClassNotFoundException{
        Connection connect = null;
        
        String url = "jdbc:mysql://localhost:3306/minilib";
        
        String username = "root";
        String password = "";
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            
            connect = DriverManager.getConnection(url, username, password);
        } catch(SQLException ex){
            System.out.println("in exec");
            System.out.println(ex.getMessage());
        }
        
        String query = "INSERT INTO `penyewa`(`ID`, `nama`, `alamat`) VALUES ('"+id+"', '"+nama+"', '"+alamat+"');";
        System.out.println(query);
        Statement st = connect.createStatement();
        st.executeQuery(query);
    }
    
}
